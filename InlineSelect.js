import React, { Component } from 'react';
import PropTypes from 'prop-types';
import StatelessSelect from '@atlaskit/single-select';

class InlineSelect extends Component {
    constructor(props) {
        super(props);
        this.updateValue = this.props.onUpdate;
        this.onSelected = this.onSelected.bind(this);
        this.defaultItemsRender = this.defaultItemsRender.bind(this);

        this.state = {
            isLoading: true,
            content: this.props.content,
            selectedItem: undefined,
            selectItems: this.defaultItemsRender(this.props.selectFrom)
        }
    }

    // This function returns an array of items for SingleSelect as specified by the API
    // See https://ak-mk-2-prod.netlify.com/packages/elements/single-select
    defaultItemsRender(selectFrom) {
        // Need to handle case when cards are loaded before epics are retrieved
        if (selectFrom === undefined) {
            return undefined;
        }

        let items = [];
        Object.keys(selectFrom).map(e => {
            let object = selectFrom[e];
            let item = {};
            item.content = object[this.props.selectValue];
            item.value = object.key;
            items.push(item);
        });
        return [{items: items}];
    }

    onSelected(selected) {
        this.updateValue(this.props.field, this.state.content);
        this.setState({content: selected.item.value});
    }

    componentWillUpdate(nextProps) {
        // TODO
        //this.state.content = nextProps.content;
        //this.state.selectItems = this.defaultItemsRender(nextProps.selectFrom);
    }

    componentWillReceiveProps(nextProps) {
        // TODO comments
        let selectedItem;
        let isLoading = true;
        let selectItems;
        if (nextProps.selectFrom !== undefined) {
            selectItems = this.defaultItemsRender(nextProps.selectFrom);
            selectedItem = selectItems[0].items.find((e) => e.value === nextProps.content);
            isLoading = false;
        }

        this.setState({
            isLoading: isLoading,
            content: nextProps.content,
            selectedItem: selectedItem,
            selectItems: selectItems
        });
    }

    render() {

        console.log(this.state.isLoading); // TODO
        console.log(JSON.stringify(this.state.selectedItem));
        console.log(JSON.stringify(this.state.selectItems));

        return (
            <div>
                <StatelessSelect
                    isLoading={this.state.isLoading}
                    items={this.state.selectItems}
                    onSelected={e => this.onSelected(e)}
                    hasAutocomplete={true}
                    droplistShouldFitContainer={false}
                    selectedItem={this.state.selectedItem}
                    placeholder={this.props.placeholder}
                />
            </div>
        );
    }
}

InlineSelect.propTypes = {
    content: PropTypes.string, // Content to display
    placeholder: PropTypes.string.isRequired, // Content to display when nothing is selected
    field: PropTypes.string, // Field name to update on change
    onUpdate: PropTypes.func.isRequired, // Function to call when an item is selected
    selectFrom: PropTypes.object, // List of items to select from
    selectValue: PropTypes.string.isRequired // Field to display from items in selectFrom
};

export default InlineSelect
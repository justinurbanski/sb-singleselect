import React, { Component } from 'react'
import { connect } from "react-redux";
import * as BacklogActions from '../../actions/BacklogActions'
import { bindActionCreators } from "redux";
import TextField from "./fields/TextField";
import InlineSelect from "./fields/InlineSelect";
import {
    estimationCustomRender, issueIdCustomRender, issueTypeCustomRender
} from "../CustomRenders";

class IssueCardContent extends Component {
    constructor(props) {
        super(props);

        this.updateFieldSummary = this.updateFieldSummary.bind(this);
        this.updateFieldEstimate = this.updateFieldEstimate.bind(this);
        this.updateFieldEpic = this.updateFieldEpic.bind(this);

        this.estimateCustomFieldId = this.props.config.estimateCustomFieldId;
        this.boardId = this.props.config.boardId;
        this.state = {content: this.props.content}
    }

    updateFieldSummary(field, value) {
        this.props.actions.updateFieldSummary(value, this.props.issueId)
    }

    updateFieldEstimate(field, value) {
        this.props.actions.updateFieldEstimate(value, this.props.issueId, this.estimateCustomFieldId, this.boardId)
    }

    updateFieldEpic() {
        // TODO
    }

    componentWillUpdate(nextProps) {
        this.state.content = nextProps.content
    }

    render() {
        let issue = this.state.content;

        const estimateString = this.getEstimate(issue);
        const summary = IssueCardContent.getSummary(issue);
        const type = IssueCardContent.getType(issue);
        const epic = IssueCardContent.getEpicKey(issue);

        return (
            <div>
                <div className="jb-card-first-row">
                    <div className="jb-card-issue-type">
                        <div className="jb-inline-edit" style={{}}>
                            <TextField editable={false}
                                       field="issueType"
                                       content={type}
                                       valueRender={issueTypeCustomRender}
                            />
                        </div>
                    </div>
                    <div className="jb-card-row-elements-group" style={{}}>
                        <div className="jb-inline-element" style={{width: 800}}>
                            <TextField
                                editable={true}
                                field="summary"
                                content={summary}
                                onUpdate={this.updateFieldSummary}
                                className="jb-summary-view"
                            />
                        </div>
                        <div className="jb-inline-element jb-field-offset" style={{width: 54}}>
                            <TextField
                                editable={false}
                                field="issueId"
                                content={this.props.issueId}
                                valueRender={issueIdCustomRender}
                            />
                        </div>
                        <div className="jb-inline-element" title="Story Points" style={{width: 60}}>
                            <TextField display={this.estimateCustomFieldId !== null}
                                       editable={true}
                                       numeric={true}
                                       field="estimation"
                                       content={estimateString}
                                       onUpdate={this.updateFieldEstimate}
                                       valueRender={estimationCustomRender}
                            />
                        </div>
                    </div>
                </div>
                <div className="jb-card-second-row">
                    <div className="jb-inline-element jb-epic">
                        <InlineSelect
                            content={epic}
                            field="epics"
                            placeholder={"No Epic"}
                            onUpdate={this.updateFieldEpic}
                            selectFrom={this.props.epics}
                            selectValue="name"
                        />
                    </div>
                </div>
            </div>
        )
    }

    static getSummary(issue) {
        return issue.issue.fields.summary;
    }

    getEstimate(issue) {
        const estimate = this.getEstimateField(issue);
        return estimate == null ? "-" : estimate.toString();
    }

    getEstimateField(issue) {
        return issue.issue.fields[this.estimateCustomFieldId];
    }

    static getType(issue) {
        return issue.issue.fields.issuetype.iconUrl;
    }

    static getEpicKey(issue) {
        const epic = issue.issue.fields.epic;
        return epic === null ? "" : epic.key;
    }
}


function mapStateToProps(state, props) {
    return {
        issues: state.issuesState.issues,
        config: state.configState.boardConfig,
        epics: state.epicsState.epics
    }
}

function mapDispatchToProp(dispatch) {
    return {actions: bindActionCreators(BacklogActions, dispatch)}
}


export default connect(mapStateToProps, mapDispatchToProp)(IssueCardContent)